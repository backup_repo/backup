if exists("b:current_syntax")
  finish
endif

" syn match logMsgNumber      "\<[-]\{0,1\}[0-9]\+\>" contained display
" syn match logMsgHexNumber   "0[xX][0-9a-fA-F]\+" contained display
" syn match logMsgIpAddr      "[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}" contained display
" syn match logMsgParam       "[[:alnum:]]\+[:=]\+[ ]\{0,1\}[-]\{0,1\}[0-9]"me=e-1 contained nextgroup=msgNumber,msgHexNumber display
" syn match logMsgBracket     "[\[\](){}]" contained display
" syn match logMsgBrackets    "[\[{(].\{-,\}[\])}]" contained contains=msgHexNumber,msgNumber,msgIpAddr,msgBracket display
" syn match EnvPrioTimeNormal "prioQueueTime: [0-9]\{1,3\} (us)" contained display
" syn match EnvPrioTimeWarn   "prioQueueTime: \([0-9]\{4,5\}\|[1234][0-9]\{5\}\) (us)" contained display
" syn match EnvPrioTimeError  "prioQueueTime: \([56789][0-9]\{5\}\|[0-9]\{7,\}\) (us)" contained display
" syn match EnvPrioQueue1     "prioQueue: 1" contained display
" syn match EnvPrioQueue2     "prioQueue: 2" contained display
" syn match EnvPrioQueue3     "prioQueue: 3" contained display
" syn match logEnvIn          "xInEnv: [^ ]\+" contained display
" syn match logEnvOut         "xOutEnv: [^ ]\+" contained display
" syn match logEnv            "x\(In\|Out\)Env.*$" contains=EnvPrioTimeNormal,EnvPrioTimeWarn,EnvPrioTimeError,EnvPrioQueue1,EnvPrioQueue2,EnvPrioQueue3,logEnvIn,logEnvOut,logMsgParam,logMsgHexNumber,logMsgNumber,logMsgBracket,logMsgBrackets display
" syn match logTlhSend        "Sending [^ ]\+ (0x[0-9a-fA-F]\{1,4\})" contained display
" syn match logTlhReceive     "Received [^ ]\+ (0x[0-9a-fA-F]\{1,4\})" contained display
" syn match logTlh            "/TLH, .*" contained contains=logTlhSend,logTlhReceive,logMsgParam,logMsgHexNumber,logMsgNumber,logMsgBracket,logMsgBrackets display
" syn match logLrmSend        "Send 0x[0-9a-fA-F]\{1,4\} [^ ]\+" contained display
" syn match logLrmReceive     "Receive 0x[0-9a-fA-F]\{1,4\} [^ ]\+" contained display
" syn match logLrm            "/RM/.*" contained contains=logLrmSend,logLrmReceive,logMsgParam,logMsgHexNumber,logMsgNumber,logMsgBracket,logMsgBrackets display
" 
" syn match wordSend          "\(Sent\|CONMAN_RLH\)" contained display
" syn match wordReceive       "\(Rcvd\|RLH_CONMAN\)" contained display
" syn match logRlh            "\(RLH_CONMAN\|CONMAN_RLH\).*" contained contains=wordReceive,wordSend display
" 
" syn match logChangeState    "ChangeState: State changed" contained display
" syn match logTransportBearerId "\(transportBearerId: [[:digit:]]*\)" contained display
" syn match logTransactionId  "\(transactionId: -\{0,1\}[[:digit:]]*\|tr.Id=0x[[:alnum:]]*\)" contained display
" syn match logBindingId      "\(bindingId: -\{0,1\}[[:digit:]]*\|BindingId: [[:digit:]]*\)" contained display
" syn match wordSsTupC        "\(SS_TUPC\|TUP_Cm\)" contained display
" syn match logNbccRef        "\(nbccRef=[[:digit:]]*\)" contained display
" syn match logSSTupc         "\(/SS_TUPC.*\|/TUP_Cm.*\)" contained contains=wordSsTupC,wordReceive,wordSend,logBindingId,logTransactionId,logTransportBearerId,logChangeState,logNbccRef display
" 
" syn match logToamSend       "sending [^ ]\+ 0x[0-9a-fA-F]\{1,4\}" contained display
" syn match logToamReceive    "received [^ ]\+ 0x[0-9a-fA-F]\{1,4\}" contained display
" syn match logToam           "/TOAM/.*" contained contains=logToamSend,logToamReceive,logMsgParam,logMsgHexNumber,logMsgNumber,logMsgBracket,logMsgBrackets display
" " syn match logMsgOther       ".*" conceal display
" " syn match logMsg            "[^ ]\+" nextgroup=logMsgOther display
" syn match logMsgTcom        "\(/TOAM/\|/RM/\|.*/CH,\|.*/B,\|.*/CM,\|.*/DM,\|.*/E,\|.*/P,\|.*/R,\|.*/NC,\|.*/G,\|.*/TNR\|.*/Logger\|.*/TUP\|.*/SS_TUP\|.*/TLH,\).*" contains=logEnv,logToam,logLrm,logRlh,logSSTupc,logTlh,logMsgParam,logMsgHexNumber,logMsgNumber,logMsgBracket,logMsgBrackets display
" syn match logInfo           "\(INF\|DBG\)" nextgroup=logMsgTcom,logMsg display
" syn match logWarning        "WRN" nextgroup=logMsgTcom,logMsg display
" syn match logError          "ERR" nextgroup=logMsgTcom,logMsg display
" " syn match logProcess        "[^[:blank:]]\+ " nextgroup=logInfo,logWarning,logError conceal display
" syn match logProcess        "[^[:blank:]]\+ " nextgroup=logInfo,logWarning,logError display
" syn match logTime           "<[^>]\{-\}> " nextgroup=logProcess display
" syn match logCard           "[^[:blank:]]\+ " nextgroup=logTime display
" syn match logLID            "[0-9a-z][0-9a-z] " nextgroup=logCard conceal display
" syn match syslogIP          "\[[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\][[:blank:]]\+" nextgroup=logLID conceal display
" syn match syslogTime        "[0-9]\+\.[0-9]\+[[:blank:]]\+[^[:blank:]]\+[[:blank:]]\+" nextgroup=syslogIP conceal display
" syn match syslogLID         "^[0-9a-z]\+[[:blank:]]\+" nextgroup=syslogTime conceal display
" syn match CallTrace         "Call trace: .*" display
" syn match KEH               ".*<01\.01 00:00:00\.000000>.*" contains=CallTrace display
" syn match all               "NothingToCatchUpJustANameholder" contains=KEH,CallTrace,S1,S2,S3,R1,R2,R3,R4,R5err,R5wrn,R5err,R6white
" " syn match copen             "SYSLOG.*" contains=logMsgTcom display
" syn match copen             "SYSLOG.*\.\(log\|LOG\|Log\)" contains=syslogLID



syn match logMsgNumber      "\v<[-]{0,1}[0-9]+>" contained display
syn match logMsgHexNumber   "\v0[xX][0-9a-fA-F]+" contained display
syn match logMsgIpAddr      "\v[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}" contained display
syn match logMsgParam       "\v[[:alnum:]]+[:=]+[ ]{0,1}[-]{0,1}[0-9]"me=e-1 contained nextgroup=msgNumber,msgHexNumber display
syn match logMsgBracket     "\v[[](){}]" contained display
syn match logMsgBrackets    "\v[[{(].{-,}[])}]" contained contains=msgHexNumber,msgNumber,msgIpAddr,msgBracket display

syn match logMacPsFddSendWrongSyntax    "\v[Ss]en([dt].*(Ind){-}.*from)" contained display
syn match logMacPsFddSend    "\v([Ss]en([dt]|ding) +[0-9a-zA-Z_():]+|[0-9a-zA-Z_():]+ [Ss]en([dt]|ing)[^0-9a-zA-Z]?)" contained display
syn match logMacPsFddReceive "\v([Rr]eceived? +[0-9a-zA-Z_():]+|[0-9a-zA-Z_():]+ [Rr]eceived?[^0-9a-zA-Z]?)" contained display
" syn match logEnv            "\vx(In|Out)Env.*$" contains=logMsgParam,logMsgHexNumber,logMsgNumber,logMsgBracket,logMsgBrackets display
" syn match logMsgMacPsFdd    "\v(/LTE MAC/(BASE/[0-9]+/[0-9A-Z]+:[0-9A-Z]+/)?|/PUMA|/HtEnv|/Remote sicad|/Sicad|/CPsEuBase|/CPsCPsEuBase|/CellMsgDispatcher|.*/RadParams).*" contains=logMacPsFddReceive,logMacPsFddSend,logMacPsFddSendWrongSyntax,logEnv,logToam,logLrm,logRlh,logSSTupc,logTlh,logMsgParam,logMsgHexNumber,logMsgNumber,logMsgBracket,logMsgBrackets display
syn match logMsgMacPsFdd    "\v/[^ ]* {-}.*" contains=logMacPsFddReceive,logMacPsFddSend,logMacPsFddSendWrongSyntax,logEnv,logToam,logLrm,logRlh,logSSTupc,logTlh,logMsgParam,logMsgHexNumber,logMsgNumber,logMsgBracket,logMsgBrackets display
syn match logInfo           "\v(INF|DBG)" nextgroup=logMsgMacPsFdd,logMsg display
syn match logWarning        "\vWRN" nextgroup=logMsgMacPsFdd,logMsg display
syn match logError          "\vERR" nextgroup=logMsgMacPsFdd,logMsg display
syn match logProcess        "\v[^[:blank:]]+ " nextgroup=logInfo,logWarning,logError display
" syn match logTime           "\v<[^>]{-}> " nextgroup=logProcess display
" syn match logCard           "\v[^[:blank:]]+ " nextgroup=logTime display
" syn match logLID            "\v[0-9a-z][0-9a-z] " nextgroup=logCard conceal display

syn match syslogIP          "\v[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}][[:blank:]]+" nextgroup=logProcess conceal display
syn match syslogTime        "\v[0-9]+\.[0-9]+\.[0-9]+[[:blank:]]+[0-9A-Z]+:[0-9A-Z]+" nextgroup=syslogIP conceal display
syn match syslogLID         "\v^[0-9a-z]+[[:blank:]]+" nextgroup=syslogTime conceal display
syn match CallTrace         "\vCall trace: .*" display
syn match all               "\vNothingToCatchUpJustANameholder" contains=KEH,CallTrace,S1,S2,S3,R1,R2,R3,R4,R5err,R5wrn,R5err,R6white
syn match copen             "\vSYSLOG.*\.(log|LOG|Log)" contains=syslogLID

hi   link   logSend             Statement
hi   link   logReceive          Preproc

hi   link   logMacPsFddSend     logSend
hi   link   logMacPsFddSendWrongSyntax     logSend
hi   link   logMacPsFddReceive  logReceive

hi   link   logMsgParam         logMsg
hi   link   logMsgBracket       Normal
hi   link   logMsgNumber        Normal
hi   link   logMsgHexNumber     Normal

hi   link   logMsgMacPsFdd      Comment
hi   link   logMsg              Comment
hi   link   logMsgOther         Comment

hi   link   logInfo             Comment
hi   link   logWarning          Statement
hi   link   logError            Error
hi   link   logProcess          Comment
hi   link   logTime             Directory
hi   link   logCard             Comment
hi   link   logLID              Comment
hi   link   syslogIP            Directory
hi   link   syslogTime          Comment
hi   link   syslogLID           Comment
hi   link   CallTrace           TODO
hi   link   KEH                 Special
hi   link   copen            Directory



" hi   link   logSend             Statement
" hi   link   logReceive          Preproc

" hi   link   logMsgParam         logMsg
" hi   link   logMsgBracket       Normal
" hi   link   logMsgNumber        Normal
" hi   link   logMsgHexNumber     Normal
" hi   link   EnvPrioTimeNormal   Statement
" hi   link   EnvPrioTimeWarn     Type
" hi   link   EnvPrioTimeError    PreProc
" hi   link   EnvPrioQueue1       TODO
" hi   link   EnvPrioQueue2       Comment
" hi   link   EnvPrioQueue3       Error
" hi   link   logEnvIn            logReceive
" hi   link   logEnvOut           logSend
" hi   link   logEnv              Normal
" hi   link   logTlhSend          logSend
" hi   link   logTlhReceive       logReceive
" hi   link   logTlh              Comment
" hi   link   logLrmSend          logSend
" hi   link   logLrmReceive       logReceive
" hi   link   logLrm              Comment
" 
" hi   link   logChangeState      Underlined
" 
" hi   link   wordSend            Preproc
" hi   link   wordReceive         Function
" hi   link   logNbccRef          Delimiter
" hi   link   logTransportBearerId    Number
" hi   link   logBindingId        Statement
" hi   link   logTransactionId    Directory
" hi   link   wordSsTupC          Special
" hi   link   logSSTupc           Normal
" 
" hi   link   logToamSend         logSend
" hi   link   logToamReceive      logReceive
" hi   link   logToam             Comment
" hi   link   logMsgTcom          Comment
" hi   link   logMsg              Comment
" hi   link   logMsgOther         Comment
" hi   link   logInfo             Comment
" hi   link   logWarning          Statement
" hi   link   logError            Error
" hi   link   logProcess          Comment
" hi   link   logTime             Directory
" hi   link   logCard             Comment
" hi   link   logLID              Comment
" hi   link   syslogIP            Directory
" hi   link   syslogTime          Comment
" hi   link   syslogLID           Comment
" hi   link   CallTrace           TODO
" hi   link   KEH                 Special
" hi   link   copen	            Directory

let b:current_syntax = "c"

