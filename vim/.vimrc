"" sort info:
" http://jordanelver.co.uk/blog/2014/03/12/sorting-columnds-of-text-in-vim-using-sort/
":%!column -t
":%!sort -k2nr


"" This must be first, because it changes other options as side effect
set nocompatible

syntax on

"filetype on
"filetype plugin on
"filetype indent on
filetype plugin indent on

"" change the mapleader from \ to ,
let mapleader=","
"" change the console mode from : to ;
nnoremap ; :

"" clear highlighted search
nmap <silent> ,0 :nohlsearch<CR>

"" Quickly edit/reload the vimrc file
nmap <silent> <leader>ev :e $MYVIMRC<CR>
nmap <silent> <leader>sv :so $MYVIMRC<CR>

"" Use Q for formatting the current paragraph (or selection)
vmap Q gq
nmap Q gqap

""remove some scroll bars, menus from gvim
set go-=T
set go-=r
set go-=m
set go-=L
set go-=l
set go-=R
set go-=b


set backspace=indent,eol,start

"colors desert
" colorscheme lucius
colorscheme molokai

set enc=utf-8
if has('gui_running')
  set guioptions-=e     "turn of tabs
  "set guifont=DejaVu_Sans_Mono:h10
  set guifont=Terminus\ 10
endif

"" for colors in tmux
set t_Co=256


"set shiftwidth=4
"set tabstop=4
"set softtabstop=4
"set expandtab
"
"set scrolloff=10
"
"set number
"
"set nobackup
"set nowritebackup
"
"set ignorecase
"set smartcase

set hidden        " The buffer of the old file will only be hidden when you switch to the new file. When you switch back, you still have your undo history.
set nowrap        " don't wrap lines
set tabstop=1     " a tab is four spaces
set backspace=indent,eol,start
                  " allow backspacing over everything in insert mode
set number        " always show line numbers
set shiftwidth=2  " number of spaces to use for autoindenting
" set softtabstop=4
set autoindent    " always set autoindenting on
set copyindent    " copy the previous indentation on autoindenting
set expandtab
set shiftround    " use multiple of shiftwidth when indenting with '<' and '>'
set showmatch     " set show matching parenthesis
set ignorecase    " ignore case when searching
set smartcase     " ignore case if search pattern is all lowercase,
                  "    case-sensitive otherwise
set smarttab      " insert tabs on the start of a line according to
                  "    shiftwidth, not tabstop
set hlsearch      " highlight search terms
set incsearch     " show search matches as you type

set history=1000         " remember more commands and search history
set undolevels=1000      " use many muchos levels of undo
set title                " change the terminal's title
set visualbell           " don't beep
set noerrorbells         " don't beep

set wildmenu
set wildmode=list:longest
set wildignore+=*\/[ab]\/*,*.svn,*.o,*.d,*.a,*.lib,*.gc*
"set wildignore='.svn,.o,.d,.a'

set list
set listchars=tab:>.,trail:.,extends:#,nbsp:.


"set lines=40 columns=140

syntax enable

"set filetype=nsnlogTupC

"" mark 80th column
"" mark 120 column
if exists('+colorcolumn')   ">= vim 7.3 or higher
  set colorcolumn=120
else                        "< vim 7.3
  au BufWinEnter * let w:m2=matchadd('ErrorMsg', '\%>120v.\+', -1)
endif

""=============================================================================
"" KEY MAPS:
""=============================================================================
" nmap     <Leader>ft  :set ft=nsnlogTupC<CR>
nmap     <Leader>ft  :set ft=nsnlogMacPsFdd<CR>
nmap     <Leader>co  :copen<CR>
nmap     <Leader>ne  :NERDTree<CR>
nmap     <Leader>f   :NERDTreeFind<CR>
nmap     <Leader>nef :NERDTreeFocus<CR>
nmap	 <Leader>a   :Ack 
nmap	 <Leader>g   :vimgrep /\v
nnoremap <leader>c :CommandT

""vertical and horisontal split
nnoremap <leader>v <C-w>v<C-w>r
nnoremap <leader>h <C-w>s<C-w>r

""moving between open windows
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

""moving between tabs
" nnoremap <S-C-h> :tabprevious<CR>
" nnoremap <C-S-l> :tabnext<CR>

""resizing windowsmap
map <silent> <A-h> 6<C-w><
map <silent> <A-j> 6<C-w>-
map <silent> <A-k> 6<C-w>+
map <silent> <A-l> 6<C-w>>
map <silent> <A-H> 12<C-w><
map <silent> <A-J> 12<C-w>-
map <silent> <A-K> 12<C-w>+
map <silent> <A-L> 12<C-w>>

""forbid using arrows
"nnoremap <up> <nop>
"nnoremap <down> <nop>
"nnoremap <left> <nop>
"nnoremap <right> <nop>
"inoremap <up> <nop>
"inoremap <down> <nop>
"inoremap <left> <nop>
"inoremap <right> <nop>
map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>

""moving over wrapped lines
nnoremap j gj
nnoremap k gk

""delete from buffer
nmap	<Leader>bd	:bd<CR>
""load last from buffer
nmap	<Leader>bl	:b #<CR>

""http://stackoverflow.com/questions/563616/vim-and-ctags-tips-and-tricks
" C-] - go to definition
" C-T - Jump back from the definition.
" C-W C-] - Open the definition in a horizontal split
" 

"" C-\ - Open the definition in a new tab
map <C-\> :tab split<CR>:exec("tag ".expand("<cword>"))<CR>
"" A-] - Open the definition in a vertical split
map <A-]> :vsp <CR>:exec("tag ".expand("<cword>"))<CR>

" 
" After the tags are generated. You can use the following keys to tag into and tag out of functions:
" 
" Ctrl-Left_MouseClick - Go to definition
" Ctrl-Right_MouseClick - Jump back from definition


""TagBar Togle
nmap <F8> :TagbarToggle<CR>

set tags=tags
"set updatetime=2000

""Easytags
"let g:easytags_dynamic_files=2
"let g:easytags_updatetime_min=300

set nolazyredraw

""auto completion options
" set completeopt=menu,menuone,preview,longest

""size of popup menu
set pumheight=15


if &term =~ '256color'
  " disable Background Color Erase (BCE) so that color schemes
  " render properly when inside 256-color tmux and GNU screen.
  " see also http://snk.tuxfamily.org/log/vim-256color-bce.html
  set t_ut=
endif


call plug#begin('~/.vim/plugged')
" Make sure you use single quotes

" On-demand loading
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'kien/ctrlp.vim'
Plug 'mark'
Plug 'luochen1990/rainbow'
Plug 'tmhedberg/matchit'
Plug 'ervandew/supertab'
Plug 'majutsushi/tagbar'
Plug 'brookhong/cscope.vim'
"Plug 'cscope_maps.vim'
Plug 'tomtom/tcomment_vim'
Plug 'derekwyatt/vim-fswitch'
Plug 'millermedeiros/vim-statline'
Plug 'godlygeek/tabular'
Plug 'scrooloose/syntastic'
Plug 'Valloric/YouCompleteMe', { 'do': './install.py' }
" Plug 'klen/python-mode', { 'on': 'Python-mode' }
Plug 'klen/python-mode'
Plug 'easymotion/vim-easymotion'
Plug 'elzr/vim-json'



" Add plugins to &runtimepath
call plug#end()


""=============================================================================
"" FOR PLUGINS:
""=============================================================================

""python-mode
"It is due to the python-mode autocomplete.
let g:pymode_rope_complete_on_dot = 0
" Override go-to.definition key shortcut to Ctrl-]
let g:pymode_rope_goto_definition_bind = "<C-]>"
" let g:pymode_rope_extended_complete = 1
" " let g:pymode_breakpoint = 0
" let g:pymode_syntax = 1
" let g:pymode_syntax_builtin_objs = 0
" let g:pymode_syntax_builtin_funcs = 0


""NERDTree-tabs
let g:nerdtree_tabs_open_on_gui_startup = 0

""syntastic
" let g:syntastic_python_checkers = ['flake8']
" let g:syntastic_python_checkers = ['pyflakes']
" let g:syntastic_python_flake8_args = '--ignore="E501,E302,E261,E701,E241,E126,E127,E128,W801"'
" let g:syntastic_php_checkers=['php', 'phpcs', 'phpmd', 'flake8']
" let g:syntastic_php_phpcs_args="--report=csv --standard=".expand('<sfile>:p:h')."/.vim/misc/phpcs-drupal-ruleset.xml"
" let g:syntastic_php_phpmd_post_args="text ".expand('<sfile>:p:h')."/.vim/misc/phpmd-ruleset.xml"

""pydoc.vim
"let g:pydoc_cmd = "/usr/bin/pydoc"

" "pydiction
" let g:pydiction_location = '~/.vim/bundle/pydiction/complete-dict'
" let g:pydiction_menu_height = 20

""ctrlp
let g:ctrlp_max_height = 40     "Set the maximum height of the match window
let g:ctrlp_working_path_mode = '0'     "works like command-t
set wildignore+=*.pyc
set wildignore+=*_build/*
set wildignore+=*/coverage/*
nmap <leader>t :CtrlP<CR>
nmap <leader>b :CtrlPBuffer<CR>


" ""SuperTab option to check context completion
" let g:SuperTabDefaultCompletionType="context"
" let g:SuperTabContextDefaultCompletionType="<c-x><c-u>"
" let g:SuperTabLongestEnhansed=1
" let g:SuperTabLongestHighlight=1
" let g:SuperTabDefaultCompletionType = "context"
" let g:SuperTabCompletionContexts = ['s:ContextText', 's:ContextDiscover']
" let g:SuperTabContextDiscoverDiscovery = ["&completefunc:<c-x><c-u>", "&omnifunc:<c-x><c-o>"]


"" Tagbar
nmap  <leader>l :TagbarOpen j<CR>



let g:clang_auto = 1

" ""clang
" "call g:ClangUpdateQuickFix()       "recompile file on the fly (jednostka translacyjna zostaje w pamieci)
" 
" "disable auto popup
" let g:clang_complete_auto=1
" 
" "show clang errors in quickfix window
" let g:clang_complete_copen=1
" 
" if has("unix")
"     "use dll of clang, faster than bin mode
"     let g:clang_use_library=1
"     let g:clang_library_path="/home/zherman/internetRepos/build/Release+Asserts/lib/"
" else
"     let g:clang_use_library=1
"     let g:clang_library_path="c:/MinGW/bin/"
" endif
" 
" "allow for snippets
" let g:clang_snippets=1
" let g:clang_conceal_snippets=1
" set conceallevel=2
" set concealcursor=vin
" 
" "pick engine for snippets
" let g:clang_snippets_engine="clang_complete"
" 
" "complete preprocessor macros and constants
" let g:clang_complete_macros=1
" 
" "complete patterns
" let g:clang_complete_patterns=0
" 
" let g:clang_hl_errors=1
" let g:clang_close_preview=1
" " let g:clang_memory_percent=70





""rainbow_parentheses
let g:rainbow_operators = 2
au FileType c,cpp,objc,objcpp call rainbow#activate()

""FSwitch (h,hpp <-> c,cpp)

au! BufEnter *.cpp let b:fswitchdst = 'hpp,h' | let b:fswitchlocs = 'rel:../inc,../Include'
au! BufEnter *.c let b:fswitchdst = 'hpp,h' | let b:fswitchlocs = 'rel:../inc,../Include'
au! BufEnter *.hpp let b:fswitchdst = 'cpp,c' | let b:fswitchlocs = 'rel:../src,../Source'
au! BufEnter *.h let b:fswitchdst = 'cpp,c' | let b:fswitchlocs = 'rel:../src,../Source'
nmap <silent> <Leader>of :FSHere<cr>

""for pyclewn
nmap  <F5>         :Ccontinue<CR>
nmap  <F9>         :Cstep<CR>
nmap  <F10>        :Cnext<CR>
"nmap  <F7>         :call setreg('*', line('.'))<CR>:Cbreak <C-R>%:<C-R>*<CR>
nmap  <F7> :exe "Cbreak " . expand("%:p") . ":" . line(".")<CR>
nmap  <F2> :exe "Cprint " . expand("<cword>") <CR>

""for CCTree
nmap <leader>ccf :CCTreeTraceForward<CR>
nmap <leader>ccr :CCTreeTraceReverse<CR>
nmap <leader>cc- :CCTreeRecurseDepthMinus<CR>
nmap <leader>cc+ :CCTreeRecurseDepthPlus<CR>

"" Cycle through buffers in the current split
" nnoremap <Space>n :bn<CR>
nnoremap <C-n> :bn<CR>
" nnoremap <Space>p :bp<CR>
nnoremap <C-p> :bp<CR>
"" It's worth noting that if you want to use the buffer cycling, you'll also neet set hidden so that vim will allow you to cycle away from buffers which may not have been saved.
"" http://tedreed.info/setup/2012/03/07/quick-motion/

"" Replace word with contents of paste buffer
"" http://stackoverflow.com/questions/2471175/vim-replace-word-with-contents-of-paste-buffer
nmap <silent> cp "_cw<C-R>0<Esc>
""This allows for change paste motion cp{motion}
" nmap <silent> cp :set opfunc=ChangePaste<CR>g@
" function! ChangePaste(type, ...)
"     silent exe "normal! `[v`]\"_c"
"     silent exe "normal! p"
" endfunction

""=============================================================================
"" My Functions:
""=============================================================================

function! ToggleWrap()

	if !exists ("s:isWrap")
		let s:isWrap = 0
	endif

	if (s:isWrap == 0)
		set wrap
		let s:isWrap = 1
	else
		set nowrap
		let s:isWrap = 0
	endif

endfunction

function! ToggleNumber()

	if !exists ("s:isRelative")
		let s:isRelative= 0
	endif

	if (s:isRelative== 0)
		set relativenumber
		let s:isRelative= 1
	else
		set number
		let s:isRelative= 0
	endif

endfunction

function! ToggleDiff()

	if !exists ("s:isDiff")
		let s:isDiff= 0
	endif

    if (s:isDiff >= 2)
		let s:isDiff= 0
        execute "diffoff!"
        return
    endif

	if (s:isDiff < 2)
		let s:isDiff+= 1
	    execute "diffthis"
	    if (s:isDiff == 1)
            echo 'ToggleDiff: 1'
        endif
	endif

endfunction

command! ToggleWrap call ToggleWrap()
command! ToggleNumber call ToggleNumber()
"nmap <leader>wr : ToggleWrap<CR>
nmap <leader>wr : set wrap!<CR>
nmap <leader>nr : ToggleNumber<CR>
""Print dir
nmap <leader>pd : echo expand('%:h')<CR>
""Print path
nmap <leader>pp : echo @%<CR>
""Print filename
nmap <leader>pf : echo expand('%:t')<CR>
""Remove ^M
nmap <leader>md : :%s/\r//g<CR>
""Remove empty lines
nmap <leader>ld : :g/^$/d<CR>
""Remove ^M and empty lines
nmap <leader>mld  : :%s/\r//g<CR>:g/^$/d<CR>
"" this or dos2unix
nmap <leader>mld2 : :%s/\r\(\n\)/\1/g
""ToglleDiff
command! ToggleDiff call ToggleDiff()
nmap <leader>d : ToggleDiff<CR>
""parse json
nmap <leader>jo : %!python -m json.tool<CR>

""tmp
" nmap <leader>tt : CommandT C_Application/<CR>


""=============================================================================
"" Other Functions:
""=============================================================================

"" Swap 2 windows START ------------------------------------------------
function! MarkWindowSwap()
    let g:markedWinNum = winnr()
endfunction

function! DoWindowSwap()
    "Mark destination
    let curNum = winnr()
    let curBuf = bufnr( "%" )
    exe g:markedWinNum . "wincmd w"
    "Switch to source and shuffle dest->source
    let markedBuf = bufnr( "%" )
    "Hide and open so that we aren't prompted and keep history
    exe 'hide buf' curBuf
    "Switch to dest and shuffle source->dest
    exe curNum . "wincmd w"
    "Hide and open so that we aren't prompted and keep history
    exe 'hide buf' markedBuf 
endfunction

nmap <silent> <leader>1  :call MarkWindowSwap()<CR>
nmap <silent> <leader>sw :call DoWindowSwap()<CR>
"" Swap 2 windows END --------------------------------------------------

"" Print current function name (C/C++)
"" http://vim.wikia.com/wiki/Getting_name_of_the_function
map _F ma][%b%b"xyy`a:echo @x<CR>

""=============================================================================
"" Scripts:
""=============================================================================

