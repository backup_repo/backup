# source /home/zherman/antigen/antigen.zsh
source /home/zherman/apps/antigen/antigen.zsh

# Load the oh-my-zsh's library.
antigen use oh-my-zsh
#
# # Bundles from the default repo (robbyrussell's oh-my-zsh).
antigen bundle git
antigen bundle heroku
antigen bundle pip
antigen bundle lein
antigen bundle command-not-found
antigen bundle autojump
autoload -U compinit && compinit -u
antigen bundle brew
antigen bundle common-aliases
antigen bundle compleat
antigen bundle git-extras
antigen bundle git-flow
antigen bundle npm
antigen bundle osx
antigen bundle web-search
antigen bundle z
antigen bundle vi-mode
antigen bundle virtualenv
antigen bundle virtualenvwrapper

antigen bundle zsh-users/zsh-autosuggestions
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-history-substring-search ./zsh-history-substring-search.zsh
antigen bundle thewtex/tmux-mem-cpu-load
#
# # Load the theme.
antigen theme gnzh
#
# # Tell antigen that you're done.
# antigen apply
#
# # Setup zsh-autosuggestions
# source /Users/nmn/.zsh-autosuggestions/autosuggestions.zsh
# 
# # Enable autosuggestions automatically
# zle-line-init() {
#     zle autosuggest-start
#     }
# 
#     zle -N zle-line-init
#
# User specific aliases and functions

## for Solarized theme:
export ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=10'
# autoload predict-on
# predict-on

## bind k and j for VI mode
bindkey -M vicmd 'k' history-substring-search-up
bindkey -M vicmd 'j' history-substring-search-down


alias mc='mc -b'
alias extractor="$HOME/scripts/extractor.py"
alias txtSearch="$HOME/scripts/txtSearch/txtSearch.py"
alias cscopeIndexer="$HOME/scripts/cscopeIndexer/cscopeIndexer.sh"
alias goToLogs="cd $HOME/Desktop/Logs/"
alias calltrace="$HOME/scripts/other/calltrace.py"
alias Config1='dd if=/dev/dvd1 of=dvddisk1.iso'
alias sgrep='find . -type d -name .svn -prune -o -name \*.hpp -print -o -name \*.cpp -print -o -name \*.h -print -o -name \*.c -print | xargs grep'
alias _nsn_mount="$HOME/scripts/_nsn_mount.sh"
alias _my_vimChange="$HOME/scripts/_my_vimChange.sh"
alias vim74="$HOME/apps/vim74/bin/vim"
alias _my_networkStructure="cat $HOME/Documents/nsn/network_structure.txt"
alias _my_lastSctLogs='echo ~/wrling/lte/mac_onesct/`ls ~/wrling/lte/mac_onesct/|tail -1`; gvim ~/wrling/lte/mac_onesct/`ls ~/wrling/lte/mac_onesct/|tail -1`'

# nsn scripts
source ~/scripts/_nsn_scripts/_nsnrc

PATH=$PATH:$HOME/.local/bin:$HOME/bin

# PATH=$PATH:/opt/apps/eclipse
# PATH=$PATH:/opt/Citrix/ICAClient
# PATH=$PATH:/mnt/drive_D/internetRepos/build/Release+Asserts/bin/
PATH=$PATH:/home/zherman/internetRepos/build/Release+Asserts/bin/
PATH=$PATH:/home/zherman/apps/dspshrink/dspshrink_v.5.21/linux
PATH=$PATH:$HOME/apps/autojump/bin
export PATH

export PROXY_IP="10.144.1.10"
export PROXY_PORT="8080"
export http_proxy="http://$PROXY_IP:$PROXY_PORT"
export http_proxy="$PROXY_IP:$PROXY_PORT"
export https_proxy="$PROXY_IP:$PROXY_PORT"

export TERM=xterm-256color
# export TERM="rxvt-unicode-256color"
#export ICAROOT="/home/zherman/apps/ICAClient/linuxx64"

function _my_numberOfUsers {
  who| awk '{print $1}'|sort -u|wc -l
}

##autojump
#[[ -s /home/zherman/.autojump/etc/profile.d/autojump.sh ]] && source /home/zherman/.autojump/etc/profile.d/autojump.sh
#autoload -U compinit && compinit -u

#http://proxyconf.glb.nsn-net.net/proxy.pac
